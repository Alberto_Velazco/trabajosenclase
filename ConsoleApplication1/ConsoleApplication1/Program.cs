﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        /// <summary>
        /// Escribe un programa que pida números decimales mientras que el usuario escriba números mayores que el primero.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            double a,b;
                Console.WriteLine("Anota un numero");
                a = double.Parse(Console.ReadLine());
            do
            {
                Console.WriteLine("Anota otro numero");
                b = double.Parse(Console.ReadLine());
            } while (a<b) ;
            Console.WriteLine("Ya no se pueden anotar mas numeros");
            Console.ReadKey();
        }
    }
}
