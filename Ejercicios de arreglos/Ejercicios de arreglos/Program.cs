﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_de_arreglos
{
    /// <summary>
    /// 1.-  Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden ascendente.
    /// 2.- Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden descendente.
    /// 3.- Que lea 10 números por teclado, los almacene en un array y muestre la suma, resta, multiplicación y división de todos.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            herramientas.ejercicio1();
            Console.ReadKey();

            Console.WriteLine("Ejercicio 2");
            herramientas.ejercicio2();
            Console.ReadKey();
        }

    }
}
